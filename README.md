```
$ npm ci
$ npm run build
$ docker compose up
$ open http://localhost:8080/admin/
```

Username: `user`
Password: `bitnami`

Create a tenant and select unitelabs as login theme.

```
$ open http://localhost:8080/realms/<tenant>/account
```
