FROM node:18.18.0-alpine3.18 As build

USER node
WORKDIR /home/node

ARG VERSION
ENV VERSION=${VERSION}
ARG COMMIT_SHA
ENV COMMIT_SHA=${COMMIT_SHA}

COPY --chown=node:node package*.json ./
RUN npm ci

COPY --chown=node:node . .
RUN npx vite build
RUN npx ts-node scripts/build.ts

RUN npm ci --omit=dev --ignore-scripts && npm cache clean --force


FROM node:18.18.0-alpine3.18

USER node
WORKDIR /home/node

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
ENV TZ=UTC

COPY --chown=node:node --from=build /home/node/.output ./theme
