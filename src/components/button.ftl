<#macro kw component="button" outline=false class="" rest...>
  <#switch outline>
    <#case true>
      <#assign color="text-gray-900 ring-1 ring-gray-300 hover:bg-gray-50 focus-visible:outline-indigo-600">
      <#break>
    <#default>
      <#assign color="text-white bg-indigo-600 hover:bg-indigo-500 focus-visible:outline-indigo-600">
  </#switch>

  <${component}
    class="${class} ${color} rounded-md px-3 py-2 text-sm font-semibold shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2"
    <#list rest as attrName, attrValue>
      ${attrName}="${attrValue}"
    </#list>
  >
    <#nested>
  </${component}>
</#macro>
