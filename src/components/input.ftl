<#macro kw autofocus=false disabled=false invalid=false label="" name="" rest...>
  <div class="relative rounded-md px-3 pb-1.5 pt-2.5 shadow-sm ring-1 ring-inset focus-within:ring-2 <#if invalid>ring-red-300 focus:ring-red-500<#else>ring-gray-300 focus-within:ring-indigo-600</#if>">
    <label for="${name}" class="block text-xs font-medium text-gray-900">${label}</label>
    <input
      id="${name}"
      name="${name}"
      class="block w-full border-0 p-0 text-color placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 <#if invalid>pr-7 text-red-900<#else>text-gray-900</#if>"
      <#if autofocus>autofocus</#if>
      <#if disabled>disabled</#if>
      aria-invalid="${invalid?c}"
      <#if invalid>aria-describedby="${name}-error"</#if>
      <#list rest as attrName, attrValue>
        ${attrName}="${attrValue}"
      </#list>
    />
    <#if invalid>
    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" class="h-5 w-5 text-red-500">
        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-5a.75.75 0 01.75.75v4.5a.75.75 0 01-1.5 0v-4.5A.75.75 0 0110 5zm0 10a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path>
      </svg>
    </div>
    </#if>
  </div>
</#macro>
