<#macro kw>
  <#compress>
    <#if !realm.loginWithEmailAllowed>
      ${msg("login.username.label")}
    <#elseif !realm.registrationEmailAsUsername>
      ${msg("login.username_or_email.label")}
    <#else>
      ${msg("login.email.label")}
    </#if>
  </#compress>
</#macro>
