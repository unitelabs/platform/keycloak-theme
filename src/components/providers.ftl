<#import "./provider.ftl" as providerIcons />

<#macro kw providers=[]>
<div class="space-y-4">
  <div class="relative">
    <div class="absolute inset-0 flex items-center" aria-hidden="true">
      <div class="w-full border-t border-gray-300"></div>
    </div>
    <div class="relative flex justify-center">
      <span class="bg-white px-2 text-sm text-gray-500">${msg("login.identity_providers")}</span>
    </div>
  </div>

  <ul class="space-y-2">
    <#list providers as provider>
    <li>
      <a href="${provider.loginUrl}" class="flex justify-center gap-2 border p-2 rounded-md hover:bg-gray-50">
        <#if providerIcons[provider.alias]??>
          <div class="h-6 w-6">
            <@providerIcons[provider.alias] />
          </div>
        </#if>
        <span class="text-sm leading-6 font-medium text-gray-900">${provider.displayName!}</span>
      </a>
    </li>
    </#list>
  </ul>
</div>
</#macro>
