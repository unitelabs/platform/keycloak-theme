<#import "./icons/github.ftl" as githubIcon>
<#import "./icons/gitlab.ftl" as gitlabIcon>
<#import "./icons/google.ftl" as googleIcon>

<#macro github>
  <@githubIcon.kw />
</#macro>

<#macro gitlab>
  <@gitlabIcon.kw />
</#macro>

<#macro google>
  <@googleIcon.kw />
</#macro>
