<#macro kw checked=false label="" name="" rest...>
  <div class="relative flex items-start">
    <div class="flex h-6 items-center">
      <input
        <#if checked>checked</#if>
        class="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
        id="${name}"
        name="${name}"
        type="checkbox"
        <#list rest as attrName, attrValue>
          ${attrName}="${attrValue}"
        </#list>
      />
    </div>
    <label class="ml-3 text-sm leading-6 text-gray-900 font-medium" for="${name}">
      ${label}
    </label>
  </div>
</#macro>
