<#macro kw color="" component="a" size="" rest...>
  <#switch color>
    <#case "primary">
      <#assign colorClass="text-indigo-600 hover:text-indigo-500">
      <#break>
    <#default>
      <#assign colorClass="text-indigo-600 hover:text-indigo-500">
  </#switch>

  <${component}
    class="<#compress>${colorClass} inline-flex</#compress>"

    <#list rest as attrName, attrValue>
      ${attrName}="${attrValue}"
    </#list>
  >
    <#nested>
  </${component}>
</#macro>
