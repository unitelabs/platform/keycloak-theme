import "./main.css";

let version = process.env.VERSION;
if (!!process.env.COMMIT_SHA) {
  version += ` (${process.env.COMMIT_SHA})`
}
console.log(version);
