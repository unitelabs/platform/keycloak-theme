<#import "template.ftl" as layout />
<#import "components/alert.ftl" as alert />
<#import "components/checkbox.ftl" as checkbox />
<#import "components/input.ftl" as input />
<#import "components/button.ftl" as button />

<@layout.layout>
  <h1 class="text-2xl font-bold leading-7 text-gray-900 sm:truncate sm:text-3xl sm:tracking-tight">${msg("update_password.headline")}</h1>

  <form class="flex flex-col gap-y-4" method="post" action="${url.loginAction}">
    <#if message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
      <@alert.kw message=message />
    </#if>

    <input type="hidden" name="username" value="${username}" />
    <input type="hidden" name="password" />

    <!-- new password -->
    <div>
      <@input.kw
        name="password-new"
        type="password"
        label=msg("update_password.new_password.label")
        placeholder=msg("update_password.new_password.placeholder")
        autofocus=true
        invalid=messagesPerField.existsError('password')
        autocomplete="new-password"
      />

      <#if messagesPerField.existsError('password')>
      <p id="password-new-error" class="mt-2 text-sm text-red-600">
        ${kcSanitize(messagesPerField.get('password'))?no_esc}
      </p>
      </#if>
    </div>

    <!-- confirm password -->
    <div>
      <@input.kw
        name="password-confirm"
        type="password"
        label=msg("update_password.confirm_password.label")
        placeholder=msg("update_password.confirm_password.placeholder")
        invalid=messagesPerField.existsError('password-confirm')
        autocomplete="new-password"
      />

      <#if messagesPerField.existsError('password-confirm')>
      <p id="password-new-error" class="mt-2 text-sm text-red-600">
        ${kcSanitize(messagesPerField.get('password-confirm'))?no_esc}
      </p>
      </#if>
    </div>

    <!-- remember me -->
    <#if isAppInitiatedAction??>
    <div>
      <@checkbox.kw
        name="logout-sessions"
        label=msg("update_password.logout_sessions")
        checked=true
      />
    </div>
    </#if>

    <!-- submit -->
    <div class="flex space-x-4">
      <@button.kw class="flex-1" type="submit">
        ${msg("update_password.submit")}
      </@button.kw>
      <#if isAppInitiatedAction??>
      <@button.kw class="flex-1" type="submit" name="cancel-aia" value="true" outline=true>
        ${msg("update_password.cancel")}
      </@button.kw>
      </#if>
    </div>
  </form>
</div>
</@layout.layout>
