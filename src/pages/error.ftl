<#import "template.ftl" as layout />
<#import "components/alert.ftl" as alert />
<#import "components/button.ftl" as button />

<@layout.layout>
  <h1 class="text-2xl font-bold leading-7 text-gray-900 sm:truncate sm:text-3xl sm:tracking-tight">${msg("error.headline")}</h1>

  <@alert.kw message=message />

  <#if skipLink??>
  <#else>
    <#if client?? && client.baseUrl?has_content>
      <div>
        <@button.kw component="a" href=client.baseUrl class="block w-full text-center" outline=true>
          ${msg("error.back")}
        </@button.kw>
      </div>
    </#if>
  </#if>

</@layout.layout>
