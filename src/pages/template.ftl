<#import "components/icons/unitelabs.ftl" as logo />

<#macro layout>
<!DOCTYPE html>
<html>

<head>
  <title>${msg("head.title")}</title>

  <meta charset="utf-8" />
  <meta name="robots" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link rel="icon" href="${url.resourcesPath}/assets/favicon.ico" />

  <#if properties.styles?has_content>
    <#list properties.styles?split(' ') as style>
      <link rel="stylesheet" href="${url.resourcesPath}/${style}" />
    </#list>
  </#if>

  <#if properties.scripts?has_content>
    <#list properties.scripts?split(' ') as script>
      <script defer type="module" src="${url.resourcesPath}/${script}"></script>
    </#list>
  </#if>
</head>
<body class="bg-gray-100">
  <div class="app">
    <div class="flex min-w-xs">
      <div class="flex-auto flex min-h-screen max-w-full relative">
        <main id="main" class="flex-1 min-w-0 overflow-hidden flex justify-center items-center">
          <div class="w-full min-w-xs max-w-md py-4">
            <div class="flex justify-center mb-4">
              <div class="w-1/2">
                <@logo.kw />
              </div>
            </div>
          
            <div class="space-y-8 px-4 py-5 sm:p-6 bg-white shadow sm:rounded-md">
              <#nested />
            </div>
          </div>
        </main>
      </div>
    </div>
  </div>
</body>
</html>
</#macro>
