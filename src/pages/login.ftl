<#import "template.ftl" as layout />
<#import "components/alert.ftl" as alert />
<#import "components/button.ftl" as button />
<#import "components/checkbox.ftl" as checkbox />
<#import "components/input.ftl" as input />
<#import "components/link.ftl" as link />
<#import "components/providers.ftl" as providers />
<#import "components/username.ftl" as username />

<#assign username><@username.kw /></#assign>

<@layout.layout>
  <h1 class="text-2xl font-bold leading-7 text-gray-900 sm:truncate sm:text-3xl sm:tracking-tight">${msg("login.headline")}</h1>

<#if realm.password>
  <form class="flex flex-col gap-y-4" method="post" action="${url.loginAction}" onsubmit="login.disabled = true; return true;">
    <#if message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
      <@alert.kw message=message />
    </#if>

    <input type="hidden" name="credentialId" value="${auth.selectedCredential!''}" />

    <!-- username -->
    <div>
      <@input.kw
        name="username"
        type="text"
        label=username
        placeholder=msg('login.username.placeholder')
        value=(login.username)!''
        autofocus=true
        disabled=usernameEditDisabled??
        invalid=messagesPerField.existsError("username", "password")
        autocomplete=realm.loginWithEmailAllowed?string("email", "username")
      />

      <#if messagesPerField.existsError('username', 'password')>
      <p id="username-error" class="mt-2 text-sm text-red-600">
        ${kcSanitize(messagesPerField.getFirstError('username','password'))?no_esc}
      </p>
      </#if>
    </div>

    <!-- password -->
    <@input.kw
      name="password"
      type="password"
      label=msg("login.password.label")
      placeholder=msg("login.password.placeholder")
      invalid=messagesPerField.existsError("username", "password")
      autocomplete="current-password"
    />

    <!-- remember me -->
    <#if realm.rememberMe && !usernameHidden??>
      <div>
        <@checkbox.kw
          name="rememberMe"
          label=msg("login.remember")
          checked=login.rememberMe??
        />
      </div>
    </#if>

    <!-- reset credentials -->
    <#if realm.resetPasswordAllowed>
    <div>
      <@link.kw color="primary" href=url.loginResetCredentialsUrl>
        ${msg("login.password.forgot")}
      </@link.kw>
    </div>
    </#if>

    <!-- submit -->
    <div>
      <@button.kw class="block w-full" type="submit">
        ${msg("login.submit")}
      </@button.kw>
    </div>
  </form>
</#if>

<!-- identify providers -->
<#if realm.password && social.providers??>
  <@providers.kw providers=social.providers />
</#if>
</div>
</@layout.layout>
