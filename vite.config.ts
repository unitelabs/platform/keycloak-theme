import { defineConfig } from "vite";
import { viteStaticCopy } from "vite-plugin-static-copy";

export default defineConfig({
  build: {
    rollupOptions: {
      input: ["src/main.ts"],
      output: {
        entryFileNames: "resources/[name].js",
        assetFileNames: "resources/[name][extname]",
        // entryFileNames: 'resources/[name]-[hash].js',
        // assetFileNames: 'resources/[name]-[hash][extname]',
      },
    },
  },
  plugins: [
    viteStaticCopy({
      targets: [
        {
          src: "src/(components|messages)",
          dest: ".",
        },
        {
          src: "src/assets/*",
          dest: "resources",
        },
        {
          src: "src/pages/*",
          dest: ".",
        },
      ],
    }),
  ],
  define: {
    'process.env.VERSION': JSON.stringify(process.env.VERSION || process.env.npm_package_version),
    'process.env.COMMIT_SHA': JSON.stringify(process.env.COMMIT_SHA),
  }
});
