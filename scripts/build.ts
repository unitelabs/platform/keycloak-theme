import archiver from "archiver";
import { createWriteStream, existsSync, mkdirSync } from "fs";

const name = "unitelabs";
const version = process.env.VERSION || process.env.npm_package_version;
const dir = ".output";
const file = `${name}-${version}.jar`;
const path = `${dir}/${file}`;

!existsSync(dir) && mkdirSync(dir);

const output = createWriteStream(`${__dirname}/../${path}`);

const archive = archiver("zip");

archive.on("error", (error) => {
  console.error(error);
});

archive.pipe(output);

archive.append(
  JSON.stringify(
    {
      themes: [
        {
          name,
          types: ["login"],
        },
      ],
    },
    null,
    2
  ),
  { name: "META-INF/keycloak-themes.json" }
);
archive.directory("dist", `theme/${name}/login`);

archive.finalize();
