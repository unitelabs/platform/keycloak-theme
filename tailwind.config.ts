import type { Config } from "tailwindcss";
import defaultTheme from 'tailwindcss/defaultTheme';

const SPACING = {
  xs: '20rem',
  sm: '24rem',
  md: '28rem',
  lg: '32rem',
  xl: '36rem',
  '2xl': '42rem',
  '3xl': '48rem',
  '4xl': '56rem',
  '5xl': '64rem',
  '6xl': '72rem',
  '7xl': '80rem',
};

export default {
  content: ["./src/**/*.{html,ftl,js}"],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          ['Inter var', ...defaultTheme.fontFamily.sans],
          {
            fontFeatureSettings: '"case", "cv11", "ss01", "ss03"',
            fontVariationSettings: 'normal',
          },
        ],
      },
      minWidth: SPACING,
      minHeight: SPACING,
      spacing: SPACING,
    },
  },
  plugins: [require("@tailwindcss/forms")],
} satisfies Config;
